<?php

use App\Controllers\PostController;

$app->get('/posts', PostController::class . ':index');
$app->get('/posts/{id}', PostController::class . ':single');
$app->post('/posts', PostController::class . ':create');
$app->patch('/posts/{id}', PostController::class . ':update');
$app->delete('/posts/{id}', PostController::class . ':delete');
