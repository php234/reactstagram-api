<?php

$container = $app->getContainer();

// Activating routes in a subfolder
$container['db'] = function ($container) {
	$host = $container['settings']['db']['host'];
	$path = $container['settings']['db']['path'];
	
	return new PDO("$host:$path");
};