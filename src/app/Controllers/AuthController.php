<?php

namespace App\Controllers;

use App\Helpers\AuthHelper;

use PDO;

use PDOException;

use \Firebase\JWT\JWT;

class AuthController extends Controller
{

	private $key = 'clRYGpjy4wWIKffjQ2oCSBBUYXMEBw7A';

	public function login($request, $response, $args)
	{
		try {
			$sql = "
				SELECT 
					id, username, email 
				FROM 
					users
				WHERE 
					username = :username
				AND 
					password = :password
			";

			$stmt = $this->c->db->prepare($sql);

			$stmt->execute([
				':username' => $request->getParam('username'),
				':password' => $request->getParam('password')
			]);

			// Get logged in user's data
			$user = $stmt->fetch(PDO::FETCH_OBJ);

			// Checked if user's username and password exist in database
			if (empty($user)) {
				return $response->withJSON([
					'success' => false,
					'status' => 404
				]);
			}

			$token = [
				'iss' => 'utopian',
				'iat' => time(),
				'exp' => time() + 1000,
				'data' => $user
			];

			$jwt = JWT::encode($token, $this->c->settings['jwt']['key']);

			return $response->withJson([
				'success' => true,
				'status' => 200,
				'jwt' => $jwt
			]);
		} catch (PDOException $e) {
			// Catch all database errors
			return $response->withJson([
				'message' => $e->getMessage()
			]);
		}
	}

	public function register($request, $response, $args)
	{
		$user = [
			'username' => $request->getParam('username'),
			'email' => $request->getParam('email'),
			'password' => $request->getParam('password'),
			'confirm_password' => $request->getParam('confirmPassword')
		];

		$password_length = 8;
		$auth_helper = new AuthHelper();

		try {
			$sql = "
				INSERT INTO users
					(username, email, password, created_at)
				VALUES
					(:username, :email, :password, :created_at)
			";

			$stmt = $this->c->db->prepare($sql);

			// Checked if user's username exist in database
			if ($this->check_if_username_exist($user['username'])) {
				return $response->withJSON([
					'message' => 'Username already exists.',
					'status' => 500
				]);
			}

			// Check if email is correct
			if (!($auth_helper->validate_email($user['email']))) {
				return $response->withJSON([
					'message' => "Email is invalid.",
					'status' => 500
				]);
			}

			// Check if password is same with confirm password
			if (!($auth_helper->validate_confirm_password($user['password'], $user['confirm_password']))) {
				return $response->withJSON([
					'message' => "Password are not same.",
					'status' => 500
				]);
			}

			// Check if password length is greater than passed value
			if (!($auth_helper->validate_password_length($password_length, $user['password']))) {
				return $response->withJSON([
					'message' => 'Password need at least ' . $password_length . ' characters.',
					'status' => 500
				]);
			}

			$stmt->execute([
				':username' => $user['username'],
				':email' => $user['email'],
				':password' => $user['password'],
				':created_at' => date('Y-m-d H:i:s')
			]);

			return $response->withJSON([
				'message' => 'success',
				'status' => 200
			]);
		} catch (PDOException $e) {
			return $response->withJSON([
				'message' => $e->getMessage()
			]);
		}
	}

	private function check_if_username_exist($username)
	{
		$sql = "
			SELECT 
				id 
			FROM 
				users
			WHERE 
				username = :username
		";

		$stmt = $this->c->db->prepare($sql);

		$stmt->execute([
			':username' => $username
		]);

		$user = $stmt->fetch(PDO::FETCH_OBJ);

		return !empty($user);
	}
}
