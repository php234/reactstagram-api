<?php

namespace App\Controllers;

use PDO;

use PDOException;

class PostController extends Controller
{

  public function index($request, $response, $args)
  {
		try {
			$sql = "
				SELECT
					p.id,
					u.username, 
					a.image as avatar, 
					p.image, 
					p.caption,
					l.likes,
					c.comments
				FROM posts p 
				LEFT JOIN users u 
				ON p.user_id = u.id 
				LEFT JOIN avatars a 
				ON a.user_id = u.id
				LEFT JOIN (
					SELECT 
						post_id, COUNT(post_id) as `likes` 
					FROM likes
					GROUP BY post_id
				) l ON (l.post_id = p.id)
				LEFT JOIN (
					SELECT 
						post_id, COUNT(post_id) as `comments` 
					FROM comments
					GROUP BY post_id
				) c ON (c.post_id = p.id)
				ORDER BY p.id 
				DESC LIMIT 15
			";

			$data = [
				'data' => $this->c->db->query($sql)->fetchAll(PDO::FETCH_OBJ),
				'message' => 'success',
				'status' => 200
			];

			return $response->withJSON($data);
		} catch(PDOException $e) {
			return [
				'message' => $e->getMessage()
			];
		}
  }
  
  public function single($request, $response, $args)
  {
		try {
			$sql = "
				SELECT
					p.id,
					u.username, 
					a.image as avatar, 
					p.image, 
					p.caption,
					l.likes,
					c.comments
				FROM posts p 
				LEFT JOIN users u 
				ON p.user_id = u.id 
				LEFT JOIN avatars a 
				ON a.user_id = u.id
				LEFT JOIN (
					SELECT 
						post_id, COUNT(post_id) as `likes` 
					FROM likes
					GROUP BY post_id
				) l ON (l.post_id = p.id)
				LEFT JOIN (
					SELECT 
						post_id, COUNT(post_id) as `comments` 
					FROM comments
					GROUP BY post_id
				) c ON (c.post_id = p.id)
				WHERE p.id = :id
			";
			
			$stmt = $this->c->db->prepare($sql);
			
			$stmt->execute([
				':id' => $args['id']
			]);
			
			$data = [
				'data' => $stmt->fetch(PDO::FETCH_OBJ),
				'message' => 'success',
				'status' => 200
			];
			
			return $response->withJSON($data);
		} catch(PDOException $e) {
			return [
				'message' => $e->getMessage()
			];
		}
  }

  public function create($request, $response, $args)
  {
		try {
			$sql = "
				INSERT INTO posts
					(user_id, image, caption)
				VALUES
					(:_user_id, :_image, :_caption)
			";
			
			$stmt = $this->c->db->prepare($sql);
			
			$stmt->execute([
				':_user_id' => $request->getParam('userId'),
				':_image' => $request->getParam('image'),
				':_caption' => $request->getParam('caption')
			]);
			
			$data = [
				'message' => 'success',
				'status' => 200
			];
			
			return $response->withJSON($data);
		} catch (PDOException $e) {
			return $response->withJSON([
				'message' => $e->getMessage()
			]);
		}
	}
	
	public function update($request, $response, $args)
	{
		try {
			$sql = "
				UPDATE posts
				SET
					image = :image,
					caption = :caption
				WHERE id = :id 
				AND user_id = :id
			";
			
			$stmt = $this->c->db->prepare($sql);
			
			$stmt->execute([
				':image' => $request->getParam('image'),
				':caption' => $request->getParam('caption'),
				':id' => $args['id'],
				':user_id' => $request->getParam('userId')
			]);
			
			$data = [
				'message' => 'success',
				'status' => 200
			];
			
			return $response->withJSON($data);
		} catch(PDOException $e) {
			return [
				'message' => $e->getMessage()
			];
		}
	}
	
	public function delete($request, $response, $args)
	{
		try {
			$sql = "
				DELETE FROM posts
				WHERE id = :id
				AND user_id = :user_id
			";
			
			$stmt = $this->c->db->prepare($sql);
			
			$stmt->execute([
				':id' => $args['id'],
				':user_id' => $request->getParam('userId')
			]);
			
			$data = [
				'message' => 'success',
				'status' => 200,
			];
			
			return $response->withJSON($data);
		} catch (PDOException $e) {
			return [
				'message' => $e->getMessage()
			];
		}
	}
}
