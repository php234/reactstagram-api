<?php

namespace App\Helpers;

class AuthHelper
{
	public function validate_email($email)
	{
		// Check if email is valid
		return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? true : false;
	}

	public function validate_confirm_password($password, $confirmPassword)
	{
		// Check if password is same with confirmPassword
		return $password == $confirmPassword ? true : false;
	}

	public function validate_password_length($length, $password)
	{
		// Check if password length is greater than parameter's length
		return strlen($password) >= $length ? true : false;
	}
}
